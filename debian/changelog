soapyremote (0.5.2-3) unstable; urgency=medium

  * Upload to unstable

 -- Andreas Bombe <aeb@debian.org>  Tue, 07 Sep 2021 01:28:10 +0200

soapyremote (0.5.2-2) experimental; urgency=medium

  * Change SoapySDR ABI version in package names and dependencies from 0.7
    to 0.8
  * Add Rules-Requires-Root field to d/control
  * Bump Standards-Version to 4.6.0, no changes required

 -- Andreas Bombe <aeb@debian.org>  Sat, 28 Aug 2021 19:31:04 +0200

soapyremote (0.5.2-1) unstable; urgency=medium

  * New upstream version 0.5.2
  * Update copyright years for Josh Blum in debian/copyright
  * Bump Standards-Version to 4.5.0, no changes required
  * Increase debhelper compat level to 13 and use debhelper-compat
    Build-Depends instead of debian/compat file
  * Add debian/not-installed listing files expected to not be installed for
    dh_missing

 -- Andreas Bombe <aeb@debian.org>  Tue, 25 Aug 2020 01:26:06 +0200

soapyremote (0.5.1-2) unstable; urgency=medium

  * Upload to unstable
  * Bump Standards-Version to 4.4.1, no changes required

 -- Andreas Bombe <aeb@debian.org>  Sun, 20 Oct 2019 17:38:16 +0200

soapyremote (0.5.1-1) experimental; urgency=medium

  * New upstream version 0.5.1
  * Change SoapySDR ABI version in package names and dependencies from 0.6
    to 0.7
  * Add build dependency on libavahi-client-dev for new DNS-SD support
  * Adjust copyright years for Josh Blum in debian/copyright
  * Increase debhelper compat level to 12
  * Bump Standards-Version to 4.4.0, no changes required

 -- Andreas Bombe <aeb@debian.org>  Mon, 23 Sep 2019 02:29:54 +0200

soapyremote (0.4.3-1) unstable; urgency=medium

  * New upstream release
  * Adjust copyright years for Josh Blum in debian/copyright
  * Increase debhelper compat level to 11 and remove dh_installinit override
    from debian/rules
  * Switch Vcs-* headers over to repositories on salsa.debian.org
  * Bump Standards-Version to 4.1.3, no changes required
  * Use https URL for debian/copyright Format header

 -- Andreas Bombe <aeb@debian.org>  Fri, 23 Mar 2018 03:04:55 +0100

soapyremote (0.4.2-1) unstable; urgency=medium

  * New upstream release
  * Change SoapySDR ABI version in package names and dependencies from 0.5-2
    to 0.6
  * Build-Depend on versioned libsoapysdr0.6-dev instead of libsoapysdr-dev
  * Link with --as-needed
  * Adjust copyright years for Josh Blum in debian/copyright
  * Bump Standards-Version to 4.0.1, no changes required

 -- Andreas Bombe <aeb@debian.org>  Fri, 11 Aug 2017 15:08:47 -0400

soapyremote (0.3.2-1) unstable; urgency=medium

  * New upstream release
  * Remove local manpage, it was merged upstream
  * Make soapysdr-module-remote Multi-Arch: same
  * Remove --parallel from dh invocation, default in compat 10
  * Add README.Debian with information on how to increase socket buffer
    limits

 -- Andreas Bombe <aeb@debian.org>  Sun, 11 Dec 2016 02:21:28 +0100

soapyremote (0.3.1-1) unstable; urgency=medium

  * Initial release. (Closes: #838314)

 -- Andreas Bombe <aeb@debian.org>  Thu, 20 Oct 2016 02:42:10 +0200
